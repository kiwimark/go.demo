package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
)

const outputFile = "c:/temp/output.txt"
const searchString = "[ServiceContract"
const searchDir = "C:/Dev/orca.elasticsearch"

func main() {

	fmt.Println("Starting")

	fileExts := []string{".cs"}
	fileList := []string{}

	// Functions are first class citizens.
	err := filepath.Walk(searchDir, func(inputPath string, f os.FileInfo, err error) error {
		var fileExt = path.Ext(inputPath)

		// Range lets you iterate over elements in various data structures.
		// In this The first arg is the index, second the item in the collection.
		for _, ext := range fileExts {
			if ext == fileExt {
				fileList = append(fileList, inputPath)
			}
		}
		return nil
	})

	if err != nil {
		panic(err)
	}

	var files = searchFilesForServiceContract(fileList, searchString)

	writeOutputFile(outputFile, files)

	fmt.Println("Done")
}

func writeOutputFile(outputFile string, files []string) {
	fmt.Println("Writing output file: ", outputFile)

	deleteFileIfExists(outputFile)

	fileHandle, _ := os.Create(outputFile)
	writer := bufio.NewWriter(fileHandle)

	// Defer the execution of closing the file until this function returns.
	defer fileHandle.Close()

	for _, file := range files {
		writer.WriteString(file + " /\r")
	}

	writer.Flush()
}

func deleteFileIfExists(filePath string) {
	if _, err := os.Stat(filePath); os.IsExist(err) {
		os.Remove(filePath)
	}
}

func searchFilesForServiceContract(fileList []string, searchString string) []string {

	outputFileList := []string{}

	for _, file := range fileList {
		fileContents, err := ioutil.ReadFile(file)
		if err != nil {
			panic(err)
		}

		fileString := string(fileContents)

		if strings.Contains(fileString, searchString) {
			outputFileList = append(outputFileList, file)
		}
	}
	return outputFileList
}
