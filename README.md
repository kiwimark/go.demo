# GO LANG - Introduction #

Basic Introduction to the [Go Programming Language](https://golang.org/) 


### What is Go ? ###

- Free and open source programming language created at Google in 2007.
- Compiled, statically typed.
- Has built in garbage collection.
- Designed by Rob Pike and Ken Thompson (Designed the original Unix operating system, invented B programming language).
- Syntax follows C
- Compiles to machine code.




### Why use go? ###
- Base Go library does almost everything
- Tooling 
- Compiling is fast
- Concurrency support
- Deployment (docker support)




### Tooling ###

| Tool          | Description | 
| ------------- |-------------| 
| build         |       compile packages and dependencies 
| clean         |       remove object files
|  doc          |       show documentation for package or symbol
| env           |       print Go environment information
| bug           |       start a bug report
| fix           |       run go tool fix on packages
| fmt           |       run gofmt on package sources
| generate      |       generate Go files by processing source
| get           |       download and install packages and dependencies
| install       |       compile and install packages and dependencies
| list          |       list packages
| run           |       compile and run Go program
| test          |       test packages
| tool          |       run specified go tool
| version       |       print Go version
| vet           |       run go tool vet on packages


## Compilation ##
- Within seconds  
- No VM to start (Java)
- Target multiple platform



## Concurrency ##
* Go Routines - light weight async process (4k of ram)
* Channels - transfer data between go routines, sync etc.
* Mutex


## Deployment ##
* Most of the time this is a single file (unless you need other resources, configs etc)
* Each build is targeted for the host platform.  E.g x64 Linux



### Show me the code ###

* Packages
* Exported vs Unexported functions.
* Go routines
* Web Server 


### Resources ###

* [Go Users](https://github.com/golang/go/wiki/GoUsers)
* [Go Wiki](https://github.com/golang/go/wiki)
* [Go git hub](https://github.com/golang)


### Example Go Commands ###
go run 
go build
go test
go test -bench .
godoc -html /Users/Mark/Dev/intro.go/mathUtil > page.html
