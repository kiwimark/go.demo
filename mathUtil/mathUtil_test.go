package mathutil

import "testing"
import "strconv"

func TestAdd(test *testing.T) {

	// Test cases.
	cases := []struct{ x, y, want int }{
		{1, 1, 2},
		{10, 12, 22},
		{0, 0, 0},
	}

	// My amazing test :)
	for _, c := range cases {
		got := Add(c.x, c.y)

		if got != c.want {
			x := strconv.Itoa(c.x)
			y := strconv.Itoa(c.y)
			result := strconv.Itoa(got)
			expected := strconv.Itoa(c.want)

			test.Errorf("Input X:%q Y:%q  Got:%q  Wanted:%q", x, y, result, expected)
		}
	}

}
