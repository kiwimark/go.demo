package mathutil

// Add will return the first value plus the second value as an interger
func Add(x int, y int) int {
	return x + y
}

// Subtract will return the first value less the second value as an interger
func Subtract(x int, y int) int {
	return x - y
}
