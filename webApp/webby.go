package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	// Package mux implements a request router and dispatcher.
	"github.com/gorilla/mux"
)

// error response contains everything we need to use http.Error
type handlerError struct {
	Error   error
	Message string
	Code    int
}

// The model
type book struct {
	Title  string `json:"title"`
	Author string `json:"author"`
	Id     int    `json:"id"`
}

var books = make([]book, 0)

// Custom type use for handling errors and formatting responses
type handler func(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError)

// Attach the standard ServeHTTP method to our handler so the http library can call it
func (fn handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	// Call the actual handler
	response, err := fn(w, r)

	// Check for errors
	if err != nil {
		log.Printf("ERROR: %v\n", err.Error)
		http.Error(w, fmt.Sprintf(`{"error":"%s"}`, err.Message), err.Code)
		return
	}

	if response == nil {
		log.Printf("ERROR: response from method is nil\n")
		http.Error(w, "Internal server error. Check the logs.", http.StatusInternalServerError)
		return
	}

	// Convert the response into JSON
	bytes, e := json.Marshal(response)

	if e != nil {
		http.Error(w, "Error marshalling JSON", http.StatusInternalServerError)
		return
	}

	// Send and log
	w.Header().Set("Content-Type", "application/json")
	w.Write(bytes)
	log.Printf("%s %s %s %d", r.RemoteAddr, r.Method, r.URL, 200)
}

func getAllBooks(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	return books, nil
}

func getBook(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {

	// mux.Vars gets the id from the path
	requestId := mux.Vars(r)["id"]

	// Parse the id.
	id, err := strconv.Atoi(requestId)

	if err != nil {
		return nil, &handlerError{err, "Id should be an integer", http.StatusBadRequest}
	}

	result, index := getBookById(id)

	if index < 0 {
		return nil, &handlerError{nil, "Could not find book " + requestId, http.StatusNotFound}
	}

	return result, nil
}

func getBookById(id int) (book, int) {
	for i, b := range books {
		if b.Id == id {
			return b, i
		}
	}
	return book{}, -1
}

var id = 0

func createId() int {
	id += 1
	return id
}

// The main entry point to the server.
func main() {

	fs := http.Dir("web/")
	fileHandler := http.FileServer(fs)

	// Setup routes
	router := mux.NewRouter()
	router.Handle("/books", handler(getAllBooks)).Methods("GET")
	router.Handle("/books/{id}", handler(getBook)).Methods("GET")
	router.PathPrefix("/goapp/").Handler(http.StripPrefix("/goapp", fileHandler))
	http.Handle("/", router)

	// Bootstrap
	books = append(books, book{"The Great Gatsby", "F. Scott Fitzgerald", createId()})
	books = append(books, book{"1984", "George Orwell", createId()})
	books = append(books, book{"Harry Potter", "J.K. Rowling ", createId()})
	books = append(books, book{"To Kill a Mockingbird", "Harper Lee ", createId()})

	log.Printf("HTTP server is running at 127.0.0.1:8008")
	addr := fmt.Sprintf("127.0.0.1:8008")

	// Start server
	err := http.ListenAndServe(addr, nil)

	// The index.html will be served at:  http://127.0.0.1:8008/goapp/

	// If we end up here it means the HTTP server has failed to start so just log the error.
	log.Printf(err.Error())
}
