package main

import "fmt"

func main() {

	fmt.Println("Running")

	numbers := []int{1, 2, 4, 8, 16, 32, 64, 128}

	channel := make(chan int)

	// Split the numbers between two go routines.
	go sum(numbers[:len(numbers)/2], channel)
	go sum(numbers[len(numbers)/2:], channel)

	// Receive the values back from the go routines via the channel.
	x, y := <-channel, <-channel

	// We should end up with 240 and 15.
	fmt.Println(x, y)

	fmt.Println("Done")
}

func sum(values []int, returnChannel chan int) {
	sum := 0

	// Calculate the sum of the input values.
	for _, value := range values {
		sum += value
	}

	// Send the sum back to the channel
	returnChannel <- sum
}
